import unittest

import time_of_day

class TestSum(unittest.TestCase):

    def test_get_time_of_day(self):
         self.assertEqual(time_of_day.get_time_of_day(3), "Bonne nuit")
         self.assertEqual(time_of_day.get_time_of_day(9), "Café du matin")
         self.assertEqual(time_of_day.get_time_of_day(16), "Bon après midi")
         self.assertEqual(time_of_day.get_time_of_day(22), "Bonne soirée")

         self.assertNotEqual(time_of_day.get_time_of_day(22), "Bonne nuit")
         self.assertNotEqual(time_of_day.get_time_of_day(16), "Café du matin")
         self.assertNotEqual(time_of_day.get_time_of_day(9), "Bon après midi")
         self.assertNotEqual(time_of_day.get_time_of_day(3), "Bonne soirée")


if __name__ == '__main__':
    unittest.main()