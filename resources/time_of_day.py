from datetime import datetime

def get_time_of_day(hour):
    """
    Returns the correct greeting depending of the hour time that it gets in parameter. 
    """

    """
    now = datetime.now()
    time = int(now.strftime("%H"))

    if (time >= 0 and time < 6):
        return "Bonne nuit" 
    if (time >= 6 and time < 12):
        return "Café du matin"
    if (time >= 12 and time < 18):
        return "Bon après midi"

    return "Bonne soirée"
    """

    if (hour >= 0 and hour < 6):
        return "Bonne nuit" 
    elif (hour >= 6 and hour < 12):
        return "Café du matin"
    elif (hour >= 12 and hour < 18):
        return "Bon après midi"

    return "Bonne soirée"
     
print (get_time_of_day(int(datetime.now().strftime("%H"))))